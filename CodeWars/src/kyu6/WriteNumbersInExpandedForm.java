package kyu6;
//You will be given a number and you will need to return it as a string in Expanded Form. For example:
//Kata.expandedForm(12); # Should return "10 + 2"
//Kata.expandedForm(42); # Should return "40 + 2"
//Kata.expandedForm(70304); # Should return "70000 + 300 + 4"
//NOTE: All numbers will be whole numbers greater than 0.

/**
 * @author oterlemez
 *
 */
public class WriteNumbersInExpandedForm {
	public static void main(String[] args) {
		int testZahl = 12;
		String str = expandedForm(testZahl);
		System.out.println(str);
	}
	public static String expandedForm(int num) {
		StringBuilder sBuilder = new StringBuilder();
		int i = 0;
		while (Math.pow(10, i - 2) < num) {
			double zahl = (double) num % Math.pow(10, i);
			num -= zahl;
			int zahlZuInt = (int) zahl;
			if (zahlZuInt != 0) {
			sBuilder.insert(0, zahlZuInt + " + ");  // NICHT ZU EMPFEHLEN BEI GROSSEN LISTEN, DA IMMER DER GANZE ARRAY GELESEN UND UEBERSCHRIEBEN WERDEN MUSS
			}
			i++;
		}
		return sBuilder.substring(0,sBuilder.length()-3);
	}

}

//TIPP: Anstelle von 'Math.pow(10, i)' kann man in der for-Schleife statt i++ besser i*10 schreiben.