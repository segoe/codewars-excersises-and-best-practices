package kyu6;
//There is an array with some numbers. All numbers are equal except for one. Try to find it!
//
//Kata.findUniq(new double[]{ 1, 1, 1, 2, 1, 1 }); // => 2
//Kata.findUniq(new double[]{ 0, 0, 0.55, 0, 0 }); // => 0.55
//
//It�s guaranteed that array contains more than 3 numbers.
//
//The tests contain some very huge arrays, so think about performance.
//
//This is the first kata in series


/**
 * https://www.codewars.com/kata/585d7d5adb20cf33cb000235
 * @author oterlemez
 *
 */
public class FindTheUniqueNumber {
	// Make sure your class is public

	public static void main(String[] args) {
		double hi = findUniq(new double[] { 1, 1, 1, 1, 3, 1 });
		System.out.println(hi);
	}

	public static double findUniq(double arr[]) {
		for (int i = 0; i < arr.length-2; i++) {
			if ((arr[i] + arr[i+1] + arr[i+2])/3 != arr[i]) {
				if (arr[i] == arr[i+1]) return arr[i+2];
				if (arr[i] == arr[i+2]) return arr[i+1];
				if (arr[i+2] == arr[i+1]) return arr[i];
			}
		}
		return arr[0];
	}

}
