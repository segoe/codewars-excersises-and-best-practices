package kyu6;

//Find the missing letter
//Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in the array.
//You will always get an valid array. And it will be always exactly one letter be missing. The length of the array will always be at least 2.
//The array will always contain letters in only one case.
//Example:
//['a','b','c','d','f'] -> 'e' ['O','Q','R','S'] -> 'P'
//["a","b","c","d","f"] -> "e"
//["O","Q","R","S"] -> "P"
//(Use the English alphabet with 26 letters!)


/**
 * @author oterlemez
 * https://www.codewars.com/kata/5839edaa6754d6fec10000a2
 */
public class FindMissingLetter {

	public static void main(String[] args) {
		char[] test = new char[] { 'd', 'f', 'g', 'h' };
		System.out.println(findMissingLetter(test));
	}

	public static char findMissingLetter(char[] test) {
		for (int i = 0; i < test.length - 1; i++) {
			if (test[i + 1] != test[i] + 1) {

				return (char) (test[i] + 1);
			}
		}
		return ' ';
	}
}