package kyu6;
import java.util.ArrayList;
import java.util.List;

//Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.
//Rules for a smiling face:
//-Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
//-A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
//-Every smiling face must have a smiling mouth that should be marked with either ) or D.
//No additional characters are allowed except for those mentioned.
//Valid smiley face examples:
//:) :D ;-D :~)
//Invalid smiley faces:
//;( :> :} :]


/** 
 * https://www.codewars.com/kata/583203e6eb35d7980400002a
 * @author oterlemez
 *
 */
public class CountTheSmileys {
	public static void main(String[] args) {
		ArrayList<String> arr = new ArrayList<String>();
		arr.add(";D");
		arr.add(":-(");
		arr.add(";~)");

		System.out.println(countSmileys(arr));
	}

	
	public static int countSmileys(List<String> arr) {
		int counter = 0;
		for (String string : arr) {
			if (string.length() == 3) {
				char a = string.charAt(0);
				char b = string.charAt(1);
				char c = string.charAt(2);

				if ((a == ':' || a == ';') && (b == '-' || b == '~') && (c == ')' || c == 'D')) {
					counter++;
				}
			} else {
				char a = string.charAt(0);
				char b = string.charAt(1);
				if ((a == ':' || a == ';') && (b == ')' || b == 'D')) {
					counter++;
				}
			}

		}
		return counter;
	}
}
