package kyu6;
//Write a function

//
//TripleDouble(long num1, long num2)
//
//which takes numbers num1 and num2 and returns 1 if there is a straight triple of a number at any place in num1 and also a straight double of the same number in num2.
//
//If this isn't the case, return 0
//Examples
//
//TripleDouble(451999277, 41177722899) == 1 // num1 has straight triple 999s and 
//                                          // num2 has straight double 99s
//
//TripleDouble(1222345, 12345) == 0 // num1 has straight triple 2s but num2 has only a single 2
//
//TripleDouble(12345, 12345) == 0
//
//TripleDouble(666789, 12345667) == 1

/**
 * @author oterlemez
 *
 */
public class TripleTrouble {
	public static void main(String[] args) {
//		tripleDouble(1,2);
		bestPracticeTripleDouble(333333333, 12333);
		System.out.println("triple trouble");
	
	}
	
	
	public static int tripleDouble(long num1, long num2) {
		System.out.println(num1);
		System.out.println(num2);

		int tripleZahl = -1;
		int doubleZahl = -1;
		int ergebnis = 0;

		String string1 = Long.toString(num1);
		String string2 = Long.toString(num2);

		// �berpr�ft ob bei der ersten Zahl ein Wert 3 mal vorkommt und speichert ihn
		// unter tripleZahl ab, falls ja.
		for (int i = 0; i < string1.length() - 2; i++) {
			// Pr�ft ob TripleZahl existiert
			if (string1.charAt(i) == string1.charAt(i + 1) && string1.charAt(i) == string1.charAt(i + 2)) {
				doubleZahl = string1.charAt(i);
				tripleZahl = Integer.parseInt(String.valueOf(string1.charAt(i)));
				System.out.println("tripleZahl = " + tripleZahl);
			}
		}
		if (tripleZahl >= 0) {
			for (int j = 0; j < string2.length() - 1; j++) {
				if (doubleZahl == string2.charAt(j) && doubleZahl == string2.charAt(j + 1)) {
					System.out.println("double Zahl ist vorhanden");
					ergebnis = 1;
				}
			}
		}
		return ergebnis;
	}
	
	//Best Practice L�sung:
	public static int bestPracticeTripleDouble(long num1, long num2) 
	  {
	    String n1str = String.valueOf(num1);
	    String n2str = String.valueOf(num2);
	    for(int i=0;i<10;i++) {
	      String n = String.valueOf(i);
	      if( n1str.contains(n+n+n) && n2str.contains(n+n) ) return 1;
	    }
	    return 0;
	  }
	
	
	
	
	
	
	
}